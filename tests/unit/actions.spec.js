import actions from '@/store/actions'
import transactions from './fixtures/transactions'

describe('Actions', () => {
  let commit;

  beforeEach(() => {
    commit = jest.fn();
  });

  it('saves a transaction', () => {
    // arranje
    const expectedTransaction = transactions.seed(1)[0]
    delete expectedTransaction.id

    // act
    actions.CREATE_UPDATE_TRANSACTION({ commit }, expectedTransaction)

    // assert
    expect(commit).toHaveBeenCalledWith('ADD_TRANSACTION', expectedTransaction);
    expect(expectedTransaction).toHaveProperty('id');
  })

  it('removes a transaction', () => {
    // arranje
    const expectedTransaction = transactions.seed(1)[0]
    // act
    actions.REMOVE_TRANSACTION({ commit }, expectedTransaction.id)
    // assert
    expect(commit).toHaveBeenCalledWith('REMOVE_TRANSACTION', expectedTransaction.id)
  })

  it('gets all saved transactions', () => {
    // arranje
    const expectedTransactions = transactions.seed(10)
    localStorage.getItem.mockReturnValue(expectedTransactions)

    // act
    actions.FETCH_TRANSACTIONS({ commit })

    // assert 
    expect(commit).toHaveBeenCalledWith('SET_TRANSACTIONS', expectedTransactions)
  })

  it('adds fake data to application', () => {
    // act
    actions.ADD_FAKE_DATA({ commit })

    // assert 
    expect(commit).toHaveBeenCalledTimes(20)
  })
})