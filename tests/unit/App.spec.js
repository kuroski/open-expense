import Vue from 'vue'
import { shallow, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import i18n from '@/i18n'
import router from '@/router'
import state from '@/store/state'
import actions from '@/store/actions'
import getters from '@/store/getters'
import App from '@/App'

const localVue = createLocalVue()
Vue.config.isUnknownElement = () => {}
localVue.use(Vuetify)
localVue.use(Vuex)

describe('App [Base]', () => {
  let store

  const build = () => {
    const wrapper = shallow(App, {
      localVue,
      router,
      store,
      i18n,
    })

    return { wrapper }
  }

  beforeEach(() => {
    actions.FETCH_TRANSACTIONS = jest.fn()
    store = new Vuex.Store({
      state,
      actions,
      getters,
    })
  })

  it('renders App view correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('searches transactions on beforeMount', () => {
    // arranje
    build()
    // assert
    expect(actions.FETCH_TRANSACTIONS).toHaveBeenCalled()
  })
})
