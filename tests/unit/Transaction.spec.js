import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import faker from 'faker'
import i18n from '@/i18n'
import router from '@/router'
import Transaction from '@/views/Transaction'
import mutations from '@/store/mutations'
import actions from '@/store/actions'
import icons from './fixtures/icons'
import categories from './fixtures/categories'
import transactions from './fixtures/transactions'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuetify)
localVue.use(VueRouter)

describe('Transaction [View]', () => {
  let store

  const build = () => {

    const wrapper = shallow(Transaction, {
      store,
      localVue,
      i18n,
      router,
    })

    return {
      wrapper,
      vm: wrapper.vm,
      closeButton: wrapper.find('.close-button'),
      saveButton: wrapper.find('.save-button'),
      form: {
        name: wrapper.find('.name-field'),
        category: wrapper.find('.category-field'),
        icon: wrapper.find('.category-icon-field'),
        amount: wrapper.find('.amount-field'),
        type: wrapper.find('.type-field'),
      }
    }
  }

  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        categories: categories.seed(10),
        icons,
        transaction: {},
        transactions: [],
      },
      mutations,
      actions,
    })
  })

  it('renders Transaction view correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders optional fields', () => {
    // arranje
    const { form } = build()
    // assert
    expect(form.icon.exists()).toBe(true)
    expect(form.amount.exists()).toBe(true)
    expect(form.type.exists()).toBe(true)
  })

  it('shows error messages when trying to save new transaction without required values', () => {
    // arranje
    const { saveButton, form } = build()
    // act
    saveButton.trigger('click')
    // assert
    expect(form.name.classes()).toContain('input-group--error')
    expect(form.category.classes()).toContain('input-group--error')
  })

  it('shows categories list', () => {
    // arranje
    store.state.categories = categories.seed(10)
    const { wrapper, form } = build()

    // assert
    expect(wrapper.vm.categories).toBe(store.state.categories)
    form
      .category
      .findAll('.list > div')
      .wrappers
      .forEach((element, index) => {
        expect(element.text()).toBe(store.state.categories[index])
      })
  })

  it('shows icons list', () => {
    // arranje
    const { wrapper, form } = build()

    // assert
    expect(wrapper.vm.icons).toEqual(store.state.icons)
    form
      .icon
      .findAll('.list > div')
      .wrappers
      .forEach((element, index) => {
        expect(element.text()).toEqual(store.state.icons[index])
      })
  })

  it('saves transaction', () => {
    // arranje
    const { wrapper, saveButton } = build()
    wrapper.vm.$refs.form.validate = jest.fn().mockReturnValue(true)
    wrapper.setData({
      transaction: {
        name: faker.name.findName(),
        category: faker.name.firstName(),
        amount: Number(faker.finance.amount()),
        createdAt: new Date(),
      }
    })

    // act
    saveButton.trigger('click')

    // assert
    expect(store.state.transactions).toHaveLength(1)
    expect(store.state.transactions).toEqual([wrapper.vm.transaction])
  })

  it('sets form data when editing a transaction', () => {
    // arranje
    store.state.transactions = transactions.seed(10)
    localStorage.getItem.mockReturnValue(store.state.transactions)
    router.push({ name: 'transaction', params: { id: store.state.transactions[1].id } })
    const { wrapper } = build()

    // assert
    expect(wrapper.vm.transaction).toEqual(store.state.transactions[1])
  })

  it('edits an transaction', () => {
    // arranje
    store.state.transactions = transactions.seed(10)
    router.push({ name: 'transaction', params: { id: store.state.transactions[1].id } })    
    const { vm, saveButton } = build()

    vm.transaction.name = 'Changed'
    vm.transaction.category = 'ChangedCategory'
    vm.transaction.amount = '0.99'
    vm.transaction.icon = 'ChangedIcon'
    vm.transaction.type = 'ChangedType'
    
    // act
    saveButton.trigger('click')
    
    // assert
    const expectedTransaction = store.state.transactions[1]
    expect(expectedTransaction.name).toBe('Changed');
    expect(expectedTransaction.category).toBe('ChangedCategory');
    expect(expectedTransaction.amount).toBe('0.99');
    expect(expectedTransaction.icon).toBe('ChangedIcon');
    expect(expectedTransaction.type).toBe('ChangedType');
  })
})
