import api from '@/api'
import transactions from './fixtures/transactions'

describe('API', () => {
  let currentTransactions
  
  beforeEach(() => {
    currentTransactions = transactions.seed(10)
    localStorage.getItem.mockReturnValue(currentTransactions)
  })

  it('get all transactions', () => {
    // act
    const response = api.get()

    // assert
    expect(response).toBe(currentTransactions);
  })

  it('adds a transaction', () => {
    // arranje
    const transaction = transactions.seed(1)[0]
    currentTransactions.push(transaction)
    // act
    api.add(transaction)
    // assert
    expect(localStorage.setItem).toHaveBeenLastCalledWith('transactions', JSON.stringify(currentTransactions));
  })

  it('edits a transaction', () => {
    // arranje
    currentTransactions[0].name = 'Changed'
    // act
    api.update(currentTransactions[0])
    // assert
    expect(localStorage.setItem).toHaveBeenLastCalledWith('transactions', JSON.stringify(currentTransactions))
  })

  it('removes a transaction', () => {
    // act
    api.remove(currentTransactions[0].id)
    // asssert
    expect(localStorage.setItem).toHaveBeenLastCalledWith('transactions', JSON.stringify(currentTransactions))
  })
})