import { shallow, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import i18n from '@/i18n'
import router from '@/router'
import VTransactionsList from '@/components/VTransactionsList'
import VTransactionsListItem from '@/components/VTransactionsListItem'
import transactions from './fixtures/transactions'

const localVue = createLocalVue()
localVue.use(Vuetify)
localVue.use(VueRouter)

describe('VTransactionsList [Component]', () => {
  let props

  const build = () => {
    const wrapper = shallow(VTransactionsList, {
      i18n,
      router,
      localVue,
      propsData: props, 
    })
    return { 
      wrapper,
      listItems: wrapper.findAll(VTransactionsListItem),
      addButton: wrapper.find('.add-button'),
    }
  }

  beforeEach(() => {
    props = {}
  })

  it('renders VTransactionsList component correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders a list of transactions', () => {
    // arranje
    props.transactions = transactions.seed(10)
    const { listItems } = build()
    // assert
    expect(listItems).toHaveLength(props.transactions.length)
  })

  it('updates transactions items when transactions prop is updated', () => {
    // arranje
    props.transactions = transactions.seed(10)
    const { wrapper, listItems } = build()
    
    expect(listItems).toHaveLength(props.transactions.length)
    wrapper.setProps({
      transactions: transactions.seed(20)
    })

    // assert
    expect(wrapper.findAll(VTransactionsListItem)).toHaveLength(20)
  })

  it('renders a ordered list of transactions by createdAt attribute', () => {
    // arranje
    props.transactions = transactions.static()
    const expectedTransactions = transactions.static().sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
    const expectedDates = expectedTransactions.map(e => e.createdAt)

    const { wrapper, listItems } = build()
    
    // assert
    expect(wrapper.vm.sortedTransactions).toEqual(expectedTransactions)
    expect(listItems).toHaveLength(props.transactions.length)
    listItems
      .wrappers
      .forEach((item, index) =>
        expect(item.props().transaction.createdAt).toEqual(expectedDates[index]))
  })

  it('renders an empty list message with a call to action button when there is no transaction', () => {
    // arranje
    const { wrapper, addButton } = build()
    // assert
    expect(wrapper.text()).toContain(i18n.t('list.empty'))
    expect(wrapper.text()).toContain('attach_money')
    expect(addButton.exists()).toBe(true)
  })

  it('navigate to new transaction page when call to action button is clicked', () => {
    // arranje
    const { addButton } = build()

    // act
    addButton.trigger('click')

    // assert
    expect(router.currentRoute.name).toEqual('new-transaction')
  })
})