import { shallow, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import i18n from '@/i18n'
import router from '@/router'
import VTransactionsListItem from '@/components/VTransactionsListItem'
import transactions from './fixtures/transactions'
import state from '@/store/state'
import actions from '@/store/actions'
import mutations from '@/store/mutations'

const localVue = createLocalVue()
localVue.use(Vuetify)
localVue.use(Vuex)
localVue.use(VueRouter)
describe('VTransactionsListItem [Component]', () => {
  let props
  let store

  const build = () => {
    const wrapper = shallow(VTransactionsListItem, {
      i18n,
      store,
      localVue,
      propsData: props,
      router,
    })
    return { 
      wrapper,
      icon: wrapper.find('i'),
      debitAmount: wrapper.find('.list__tile__sub-title.red--text'),
      creditAmount: wrapper.find('.list__tile__sub-title.green--text'),
      deleteButton: wrapper.find('.delete-transaction')
    }
  }

  beforeEach(() => {
    state.transactions = transactions.seed(10)

    props = {
      transaction: state.transactions[1]
    }

    actions.SHOW_NEW_TRANSACTION_MODAL = jest.fn()
    mutations.SET_TRANSACTION = jest.fn()
    store = new Vuex.Store({
      state,
      actions,
      mutations,
    })
  })

  it('renders VTransactionsListItem component correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('renders category icon', () => {
    // arranje
    const { wrapper, icon } = build()
    // assert
    expect(wrapper.html()).toContain(props.transaction.icon)
    expect(icon.exists()).toBe(true)
  })

  it('renders transaction createdAt property', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.text()).toContain(i18n.d(props.transaction.createdAt, 'short'))
  })

  it('renders transaction amount', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.text()).toContain(i18n.n(props.transaction.amount, 'currency'))
  })

  it('renders transaction name', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.text()).toContain(props.transaction.name)
  })

  it('renders transaction delete button', () => {
    // arranje
    const { deleteButton } = build()
    // assert
    expect(deleteButton.exists()).toBe(true)
  })

  it('renders red amount if transaction type is debit', () => {
    // arranje
    props.transaction.type = 'debit'
    const { debitAmount } = build()
    // assert
    expect(debitAmount.exists()).toBe(true)
  })

  it('renders green amount if transaction type is credit', () => {
    // arranje
    props.transaction.type = 'credit'
    const { creditAmount } = build()
    // assert
    expect(creditAmount.exists()).toBe(true)
  })

  it('renders negative amount if type is debit', () => {
    // arranje
    props.transaction.type = 'debit'
    const { wrapper } = build()
    // assert
    expect(wrapper.text()).toContain(i18n.n(-props.transaction.amount, 'currency'))
  })

  it('calls edit transaction on item click', () => {
    // arranje
    const { wrapper } = build()
    localStorage.getItem.mockReturnValue(store.state.transactions)    
    // act
    wrapper.trigger('click')
    // assert
    expect(router.currentRoute.name).toBe('transaction')
  })

  it('deletes an transaction', () => {
    // arranje
    const id = props.transaction.id
    const { deleteButton } = build()
    // act
    deleteButton.trigger('click')
    // assert
    const searchedTransaction = store.state.transactions.find(e => e.id === id)
    expect(store.state.transactions).toHaveLength(9)
    expect(searchedTransaction).toBeUndefined()
  })
})