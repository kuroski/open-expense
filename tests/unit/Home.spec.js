import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import i18n from '@/i18n'
import router from '@/router'
import Home from '@/views/Home'
import state from '@/store/state'
import mutations from '@/store/mutations'
import getters from '@/store/getters'
import transactions from './fixtures/transactions'

import VBalance from '@/components/VBalance'
import VWeekTransactionsChart from '@/components/VWeekTransactionsChart'
import VTransactionsList from '@/components/VTransactionsList'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuetify)
localVue.use(VueRouter)

describe('Home [View]', () => {
  let store

  const build = () => {
    const wrapper = shallow(Home, {
      store,
      localVue,
      router,
      i18n,
    })

    return { 
      wrapper,
      balance: wrapper.find(VBalance),
      chart: wrapper.find(VWeekTransactionsChart),
      list: wrapper.find(VTransactionsList),
      addButton: wrapper.find('.add-transaction'),
    }
  }

  beforeEach(() => {
    state.transactions = []
    store = new Vuex.Store({
      state,
      mutations,
      getters,
    })
  })

  it('renders Home view correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('shows current balance', () => {
    // arranje
    const { balance } = build()

    // assert
    expect(balance.exists()).toBe(true)
    expect(balance.props().balance).toBe(0)
  })

  it('updates current balance prop when new transaction is added', () => {
    // arranje
    const { balance } = build()
    expect(balance.props().balance).toBe(0)

    // act
    store.commit('SET_TRANSACTIONS', [{ amount: 500, type: 'credit' }])

    // assert
    expect(balance.props().balance).toBe(500)
  })

  it('shows correct user balance values', () => {
    // arranje
    store.state.transactions = transactions.static()
    const { balance } = build()
    // assert
    expect(balance.props().balance).toBe(store.getters.balance)
  })

  it('shows a monthly transactions chart', () => {
    // arranje
    store.state.transactions = transactions.seed(10)
    const { chart } = build()

    // assert
    expect(chart.exists()).toBe(true)
    expect(chart.props().transactions)
      .toEqual(store.state.transactions)
  })

  it('updates chart transactions prop when new transation is added', () => {
    // arranje
    const expectedTransactions = transactions.seed(10)
    const { chart } = build()
    expect(chart.props().transactions).toEqual([])

    // act
    store.commit('SET_TRANSACTIONS', expectedTransactions)

    // assert
    expect(chart.props().transactions).toEqual(expectedTransactions)
  })

  it('updates chart balance prop when new transation is added', () => {
    // arranje
    const { chart } = build()
    expect(chart.props().balance).toBe(0)

    // act
    store.commit('SET_TRANSACTIONS', [{ amount: 500, type: 'credit' }])

    // assert
    expect(chart.props().balance).toBe(500)
  })

  it('shows a transactions list', () => {
    // arranje
    store.state.transactions = transactions.seed(10)
    const { list } = build()

    // assert
    expect(list.exists()).toBe(true)
    expect(list.props().transactions)
      .toBe(store.state.transactions)
  })

  it('updates list transactions prop when new transation is added', () => {
    // arranje
    const expectedTransactions = transactions.seed(10)
    const { list } = build()
    expect(list.props().transactions).toEqual([])

    // act
    store.commit('SET_TRANSACTIONS', expectedTransactions)

    // assert
    expect(list.props().transactions).toEqual(expectedTransactions)
  })

  it('navigate to new transaction page when add button is clicked', () => {
    // arranje
    const { addButton } = build()

    // act
    addButton.trigger('click')

    // assert
    expect(router.currentRoute.name).toEqual('new-transaction')
  })
})
