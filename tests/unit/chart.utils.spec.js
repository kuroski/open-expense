import chart from '@/utils/chart'
import transactions from './fixtures/transactions'

describe('Chart utils', () => {
  it('generates dataset for chart.js component', () => {
    // arranje
    const expectedDataSet = [ 385, -341, -1079, -1889, -1385 ]
    const currentTransactions = transactions.static()

    // act
    const dataSet = chart.buildChartDataset(0, currentTransactions)

    // assert
    expect(dataSet).toMatchObject(expectedDataSet)
  })
})