import getters from '@/store/getters'
import transactions from './fixtures/transactions'

describe('Getters', () => {
  let state

  beforeEach(() => {
    state = {
      transactions: [],
    }
  })

  it('shows current user balance', () => {
    // arranje
    state.transactions = transactions.static()
    // act
    const balance = getters.balance(state)
    //assert
    expect(balance).toBe(-1385)
  })

  it('converts transactions amount to Number if String is passed', () => {
    // arranje
    state.transactions = transactions.seed(1)
    state.transactions[0].amount = '10.00'
    state.transactions[0].type = 'credit'
    // act
    const balance = getters.balance(state)
    //assert
    expect(balance).toBe(10)
  })
})