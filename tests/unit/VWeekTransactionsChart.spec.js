import { shallow, createLocalVue } from '@vue/test-utils'
import Trend from 'vuetrend'
import i18n from '@/i18n'
import chart from '@/utils/chart'
import transactions from './fixtures/transactions'

import VWeekTransactionsChart from '@/components/VWeekTransactionsChart'

const localVue = createLocalVue()
localVue.use(Trend)

describe('VWeekTransactionsChart [Component]', () => {
  let props

  const build = () => {
    const wrapper = shallow(VWeekTransactionsChart, { 
      i18n, 
      localVue,
      propsData: props, 
      attachToDocument: true,
    })

    return { 
      wrapper,
      vm: wrapper.vm, 
    }
  }

  beforeEach(() => {
    props = {
      balance: 0,
      transactions: [],
    }
  })

  // TODO: SVG method bug on lib: https://github.com/QingWei-Li/vue-trend/issues/20
  xit('renders VWeekTransactionsChart view correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  // TODO: SVG method bug on lib: https://github.com/QingWei-Li/vue-trend/issues/20
  xit('renders empty line if chart dont have minimum data to render', () => {
    // arranje
    const { vm } = build()
    // assert
    expect(vm.dataSet).toEqual([0, 0])
  })

  it('updates chart dataSet when props are changed', () => {
    // arranje
    const currentTransactions = transactions.static()
    const expectedDataSet = chart.buildChartDataset(100, currentTransactions)

    const { wrapper, vm } = build()
    expect(vm.dataSet).toEqual([0, 0])
    
    // act
    wrapper.setProps({
      balance: 100,
      transactions: currentTransactions
    })

    // assert
    expect(vm.dataSet).toEqual(expectedDataSet)
  })
})