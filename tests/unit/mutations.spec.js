import mutations from '@/store/mutations'
import transactions from './fixtures/transactions'

describe('Mutations', () => {
  it('sets transactions', () => {
    // arranje
    const state = { transactions: [] }
    const expectedTransations = transactions.seed(5)

    // act
    mutations.SET_TRANSACTIONS(state, expectedTransations)

    // assert
    expect(state.transactions).toHaveLength(expectedTransations.length)
    expect(state.transactions).toEqual(expectedTransations)
    expect(state.transactions).not.toBe(expectedTransations)
  })
  it('adds a new transaction', () => {
    // arranje
    const expectedTransation = transactions.seed(1)[0]
    const state = { transactions: [] }
    // act
    mutations.ADD_TRANSACTION(state, expectedTransation)
    // assert
    expect(state.transactions).toHaveLength(1)
    expect(state.transactions[0]).toEqual(expectedTransation)
    expect(state.transactions[0]).not.toBe(expectedTransation)
  })

  it('edit a transaction', () => {
    // arranje
    const newTransactions = transactions.seed(5)
    const expectedTransation = {...newTransactions[0]}
    const state = {
      transactions: newTransactions,
      transaction: expectedTransation
    }
    expectedTransation.amount = '7.000'
    expectedTransation.name = 'Changed'

    // act
    mutations.EDIT_TRANSACTION(state, expectedTransation)

    // assert
    expect(state.transactions[0]).toEqual(expectedTransation)
    expect(state.transactions[0]).not.toBe(expectedTransation)
  })
  
  it('sets transaction to be edited', () => {
    // arranje
    const expectedTransation = transactions.seed(1)[0]
    const state = { transaction: {} }
    // act
    mutations.SET_TRANSACTION(state, expectedTransation)
    // assert
    expect(state.transaction).toEqual(expectedTransation)
    expect(state.transaction).not.toBe(expectedTransation)
  })
  
  it('removes a transaction', () => {
    // arranje
    const state = { 
      transactions: transactions.seed(10) 
    }
    const expectedTransation = state.transactions[1]
    // act
    mutations.REMOVE_TRANSACTION(state, expectedTransation.id)
    // assert
    expect(state.transactions).toHaveLength(9)
    expect(state.transactions).not.toContain(expectedTransation)
  })
})