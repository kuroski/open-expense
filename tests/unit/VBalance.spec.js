import { shallow, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import i18n from '@/i18n'
import VBalance from '@/components/VBalance'

const localVue = createLocalVue()
localVue.use(Vuetify)

describe('VBalance [Component]', () => {
  let props

  const build = () => {
    const wrapper = shallow(VBalance, {
      i18n,
      localVue,
      propsData: props
    })
    return { wrapper }
  }

  beforeEach(() => {
    props = {}
  })
  it('renders VBalance component correctly', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('shows current balance', () => {
    // arranje
    const { wrapper } = build()
    // assert
    expect(wrapper.text()).toContain(i18n.n(0, 'currency'))
  })

  it('updates current balance when new transaction is added', () => {
    // arranje
    const { wrapper } = build()
    expect(wrapper.text()).toContain(i18n.n(0, 'currency'))
    
    // act
    wrapper.setProps({ balance: 500 })

    // assert
    expect(wrapper.text()).toContain(i18n.n(500, 'currency'))
  })

  it('shows red icon indicator when balance is negative', () => {
    // arranje
    props.balance = -100
    const { wrapper } = build()

    // assert
    expect(wrapper.text()).toContain('trending_down')
    expect(wrapper.find('.badge__badge').classes()).toContain('red')
  })

  it('shows green icon indicator when balance is positive', () => {
    // arranje
    props.balance = 100
    const { wrapper } = build()

    // assert
    expect(wrapper.text()).toContain('trending_up')
    expect(wrapper.find('.badge__badge').classes()).toContain('green')
  })

  it('shows grey icon indicator when balance is zero', () => {
    // arranje
    const { wrapper } = build()

    // assert
    expect(wrapper.text()).toContain('trending_flat')
    expect(wrapper.find('.badge__badge').classes()).toContain('grey')
  })
})