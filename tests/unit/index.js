import Vue from 'vue';

const app = document.createElement('div');
app.setAttribute('data-app', true);
document.body.append(app);

Vue.config.productionTip = false;