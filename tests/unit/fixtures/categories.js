import faker from 'faker'

export default {
  seed: (amount) => new Array(amount)
    .fill()
    .map(() => faker.name.firstName()),
  static: () => ['Educação', 'Casa', 'Salário', 'Transferência', 'Pet', 'Aniversário']
}