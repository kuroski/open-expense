import faker from 'faker'
import icons from './icons'
import categories from './categories';

export default {
  seed: (amount) => new Array(amount)
    .fill()
    .map(() => ({
      id: faker.random.uuid(),
      name: faker.name.findName(),
      category: faker.random.arrayElement(categories.static()),
      icon: faker.random.arrayElement(icons),
      amount: faker.finance.amount(),
      type: faker.random.arrayElement(['credit', 'debit']),
      createdAt: faker.date.recent(30),
      updatedAt: faker.date.recent(30),
    })),
  static: () => [
    {
      amount: 385,
      type: 'credit',
      createdAt: new Date('2018-04-05T20:52:09.133Z'),
    },
    {
      amount: 726,
      type: 'debit',
      createdAt: new Date('2018-04-16T17:52:31.102Z'),
    },
    {
      amount: 738,
      type: 'debit',
      createdAt: new Date('2018-03-30T22:12:48.588Z'),
    },
    {
      amount: 810,
      type: 'debit',
      createdAt: new Date('2018-03-23T13:28:42.553Z'),
    },
    {
      amount: 504,
      type: 'credit',
      createdAt: new Date('2018-04-02T16:19:10.477Z')
    },
  ]
}
