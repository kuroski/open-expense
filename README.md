## OpenExpense

Controle as transações da sua carteira.
Demo: https://open-expense.herokuapp.com/#/

### Instalação

``` sh
# instale as dependências
npm install

# rode o servidor
npm run serve

# execute os testes
npm run test

# veja o coverage de testes
npm run coverage

# para gerar um novo build
npm run build
```

### Tecnologias utilizadas
- [Vue.js](https://vuejs.org/)
- [Vuex](https://vuex.vuejs.org/en/)
- [Vue Test Utils](https://vue-test-utils.vuejs.org/en/)
- [Vue Router](https://router.vuejs.org/en/)
- [Vuetify](https://vuetifyjs.com/en/)
- [Vue Trend](https://github.com/QingWei-Li/vue-trend)
- [Moment](https://momentjs.com/)
- [Store](https://github.com/marcuswestin/store.js/)
- [uuid](https://www.npmjs.com/package/uuid)
- [Jest](https://facebook.github.io/jest/)

### Motivações
Foi utilizado Vue.js por ser uma das minhas ferramentas favoritas para construção de aplicações.

A princípio eu prefiro construir as aplicações da forma mais pura possível, e só a partir de uma necessidade adicionar uma ferramenta, mas neste caso eu achei interessante fazer uso do Vue até para um incentivo ao próprio estudo.

### Outros

Foram criados em torno de 62 testes unitários, sendo que 2 deles estarão como `skipped` devido a problemas de bibliotecas de terceiros.

E também todo o processo foi feito a partir do CI do Gitlab, e o site é feito o deploy de forma automática no Heroku.

### Como foi idealizada a aplicação

#### Papel
1. Foi anotado em papel as funcionalidades basicas da aplicação, e elencadas possíveis extras
2. Foi levantado softwares de concorrentes
3. Foram desenhados possíveis protótipos das telas

![foto do papel](https://image.ibb.co/bNzB1x/IMG_20180410_234443422.jpg)

#### Virtual
1. Tendo a base do que o sistema iria precisar, primeiro foi feito um levantamento de uma possível primeira definição do `state` da aplicação
2. A partir daí foi criada a base do projeto com o `vue-cli`
3. Todas as funcionalidades foram desenvolvidas tendo testes como ponto principal e a maior parte do projeto foi feito utilizando TDD

### Dificuldades técnicas
Houve alguns contratempos durante o desenvolvimento, principalmente por problemas de bibliotecas de terceiros.

Se rodarem os testes, vocês verão que são exibidos vários alertas por parte de componentes do Vuetify, a princípio, parece que devido a última beta do `vue-test-utils` as pessoas no geral estão com este mesmo problemas.

Isto dificultou um pouco o processo, pois a quantidade de warnings no console deixava a leitura complexa, eu precisava analizar linha por linha para achar o meu `console.log` ou para analizar qual problema estava ocorrendo.

Também acabei empacando algumas horas com coisas muito basicas, que acabaram tomando muito tempo (um assert feito de forma incorreta, ou algum erro genérico).

Tive problemas também com a biblioteca `Vue Trend`, que acabei tendo que abrir uma [issue](https://github.com/QingWei-Li/vue-trend/issues/20) devido a uma alteração das APIs de SVG do Javascript.

Começando a próxima semana, devo ajudar o dono do repositório a trabalhar na correção da issue.