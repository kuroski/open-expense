export default {
  'pt-BR': {
    addFakeData: 'Adicionar dados de demonstração',
    list: {
      empty: 'As coisas andam meio vazias por aqui, o que acha de fazer uma transação?'
    },
    form: {
      save: 'Salvar',
      name: 'Nome',
      category: 'Categoria',
      icon: 'Icone da categoria',
      amount: 'Valor',
      credit: 'Crédito',
      debit: 'Débito',
      required: 'O campo é obrigatório',
      amountMaxLength: 'Não é possível que você queira usar essa quantidade de dinheiro, ou você está muito endividado ou alguma instituição pode estar precisando de uma doação, não acha?',
    }
  }
}