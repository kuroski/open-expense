import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment'
import messages from './messages'
import dateTimeFormats from './dateTimeFormats'
import numberFormats from './numberFormats'

Vue.use(VueI18n)

moment.locale('pt-BR')

export default new VueI18n({
  locale: 'pt-BR',
  fallbackLocale: 'pt-BR',
  messages,
  dateTimeFormats,
  numberFormats,
})
