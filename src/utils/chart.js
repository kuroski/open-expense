import moment from 'moment'

export default {
  buildChartDataset(amount = 0, transactions = []) {
    let currentAmount = Number(amount)
    const dataSet = transactions.reduce((acc, actual) => {
      if(actual.type === 'credit') currentAmount += Number(actual.amount)
      else currentAmount -= Number(actual.amount)

      return {
        ...acc,
        [moment(actual.createdAt).format('DD MMM')]: Number(currentAmount.toFixed(2))
      }
    }, {})

    const values = Object.values(dataSet)
    if (values.length < 2) return [0, 0]
    return values
  }

}