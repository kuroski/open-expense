import icons from '@/../tests/unit/fixtures/icons'
import categories from '@/../tests/unit/fixtures/categories'

export default {
  transaction: {},
  transactions: [],
  categories: categories.static(),
  icons,
  loading: false,
}