import Vue from 'vue'

export default {
  SET_TRANSACTIONS: (state, transactions) => {
    state.transactions = state.transactions.splice(0).concat(transactions)
  },
  SET_TRANSACTION: (state, transaction) => {
    state.transaction = {...transaction}
  },
  ADD_TRANSACTION: (state, transaction) => {
    state.transactions.push({...transaction})
  },
  EDIT_TRANSACTION: (state, transaction) => {
    const key = state.transactions.findIndex(e => e.id === transaction.id)
    Vue.set(state.transactions, key, {...transaction})
  },
  REMOVE_TRANSACTION: (state, id) => {
    const key = state.transactions.findIndex(e => e.id === id)
    state.transactions.splice(key, 1)
  }
}
