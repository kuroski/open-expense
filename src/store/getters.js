export default {
  balance: (state) => {
    return state.transactions.reduce((acc, actual) => {
      if (actual.type === 'credit') return acc + Number(actual.amount)
      return acc - Number(actual.amount)
    }, 0)
  }
}
