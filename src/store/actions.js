import uuid from 'uuid/v4'
import transactions from '@/../tests/unit/fixtures/transactions'
import api from '@/api'

export default {
  ADD_FAKE_DATA: ({ commit }) => {
    const transactionsToCreate = transactions.seed(20)
    transactionsToCreate.forEach((entity) => {
      commit('ADD_TRANSACTION', entity)
      api.add(entity)
    })
  },
  CREATE_UPDATE_TRANSACTION: ({ commit }, transaction) => {
    if (transaction.id) {
      transaction.updatedAt = new Date()
      commit('EDIT_TRANSACTION', transaction)
      api.update(transaction)
    } else {
      transaction.createdAt = new Date()
      transaction.id = uuid()
      commit('ADD_TRANSACTION', transaction)
      api.add(transaction)
    }
  },

  REMOVE_TRANSACTION: ({ commit }, id) => {
    commit('REMOVE_TRANSACTION', id)
    api.remove(id)
  },

  FETCH_TRANSACTIONS: ({ commit }) => {
    commit('SET_TRANSACTIONS', api.get())
  }
}
