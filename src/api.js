import store from 'store'

export default {
  get() {
    return store.get('transactions') || []
  },
  update(transaction) {
    const transactions = store.get('transactions') || []
    let index = transactions.findIndex(e => e.id === transaction.id)
    transactions[index] = transaction
    store.set('transactions', transactions)
  },
  
  add(transaction) {
    const transactions = store.get('transactions') || []
    transactions.push(transaction)
    store.set('transactions', transactions)
  },
  
  remove(id) {
    let transactions = store.get('transactions') || []
    const index = transactions.findIndex(e => e.id === id)
    transactions.splice(index, 0)
    store.set('transactions', transactions) 
  },
}
