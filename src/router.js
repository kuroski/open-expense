import Vue from 'vue'
import VueRouter from 'vue-router'
import store from 'store'
import Home from './views/Home'
import Transaction from './views/Transaction'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/new-transaction',
      name: 'new-transaction',
      component: Transaction,
    },
    {
      path: '/transaction/:id',
      name: 'transaction',
      component: Transaction,
      beforeEnter: (to, from, next) => {
        const transactions = store.get('transactions') || []
        if (!transactions.find(e => e.id === to.params.id)) 
          next({ name: 'new-transaction' })
        else 
          next()
      }
    },
  ]
})
